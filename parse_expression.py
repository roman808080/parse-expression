def parse_number(string):
    number_string = ''

    for symbol in string.replace(' ', ''):
        if not symbol.isdigit() and symbol != '.':
            break
        number_string += symbol

    return number_string


def get_position_for_signs(expression, signs):
    positions = [expression.find(sign) for sign in signs if expression.find(sign) != -1]
    minimum_position = len(expression)

    if len(positions) == 0:
        return None

    for position in positions:
        if position < minimum_position:
            minimum_position = position

    return minimum_position


def get_position(expression):
    signs_groups = [['*', '/'], ['+', '-']]

    for signs_group in signs_groups:
        position = get_position_for_signs(expression, signs_group)
        if position is not None:
            return position

    return None


def parse_first_number(position, expression):
    first_number = None

    if position == 0:

        privios_position = position
        position = get_position(expression[1:])
        if position:
            position += 1

        first_number = expression[privios_position: position]

    if position is None:
        return (None, None)

    if first_number is None:
        first_number = parse_number(expression[:position][::-1])[::-1]

    return (position, first_number)


def find_closing_round_bracket(expression):
    non_closed_brackets = 0
    position = 0

    for symbol in expression:
        if symbol == '(':
            non_closed_brackets += 1

        if symbol == ')':
            non_closed_brackets -= 1

        if non_closed_brackets == 0 and expression[position] == ')':
            return position

        position += 1

    return -1


def parse_expression_with_round_brackets(expression):
    first_bracket = expression.find('(')

    while first_bracket != -1:
        last_bracket_position = find_closing_round_bracket(expression[first_bracket:])
        last_bracket = last_bracket_position + len(expression[:first_bracket])
        part_of_expression = expression[first_bracket: last_bracket + 1]
        result_of_part_expression = str(parse_expression(part_of_expression[1:-1]))

        expression = expression.replace(part_of_expression, result_of_part_expression, 1)
        first_bracket = expression.find('(')

    return expression


def parse_expression(expression):
    expression = expression.replace(' ', '')
    if not expression:
        return 0
    if '(' in expression:
        expression = parse_expression_with_round_brackets(expression)

    position = get_position(expression)

    decisions = {'+': lambda x, y: x + y,
                 '-': lambda x, y: x - y,
                 '/': lambda x, y: x / y,
                 '*': lambda x, y: x * y}

    position, first_number = parse_first_number(position, expression)
    if position is None:
        return float(expression)

    second_number_expression = expression[position:][1:]
    second_nuber_sign = '-' if second_number_expression[0] == '-' else ''
    if second_number_expression[0] == '-':
        second_number_expression = second_number_expression[1:]

    second_number = parse_number(second_number_expression)
    second_number_float = float(second_number) if len(second_nuber_sign) == 0 else float(second_number) * -1

    part_of_expression = (
        first_number + expression[position] + second_nuber_sign + second_number)
    result_of_part_expression = str(
        decisions[expression[position]](float(first_number),
                                        second_number_float))

    return parse_expression(expression.replace(part_of_expression,
                            result_of_part_expression, 1))
