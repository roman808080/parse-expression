from unittest import TestCase
from parse_expression import parse_expression, parse_number


class ParseTimeTests(TestCase):
    def test_parse_number(self):
        self.assertEqual(parse_number('3'), '3')
        self.assertEqual(parse_number('3.3'), '3.3')
        self.assertEqual(parse_number('3.3-'), '3.3')
        self.assertEqual(parse_number(' 3.3'), '3.3')

    def test_parse_empty(self):
        self.assertEqual(parse_expression(''), 0)

    def test_parse_expression_from_number(self):
        self.assertEqual(parse_expression('0'), 0)
        self.assertEqual(parse_expression('1'), 1)
        self.assertEqual(parse_expression(' 1 '), 1)

    def test_simple_expression_with_plus(self):
        self.assertEqual(parse_expression('2+3'), 5)
        self.assertEqual(parse_expression(' 2+ 3'), 5)
        self.assertEqual(parse_expression('2 + 3'), 5)
        self.assertEqual(parse_expression('2+3+8'), 13)
        self.assertEqual(parse_expression('+3'), 3)

    def test_simple_expression_with_minus(self):
        self.assertEqual(parse_expression('3-4'), -1)
        self.assertEqual(parse_expression('-3-4'), -7)
        self.assertEqual(parse_expression('-1'), -1)

    def test_simple_expression_with_plus_and_minus(self):
        self.assertEqual(parse_expression('3+5-9'), -1)
        self.assertEqual(parse_expression('3-5+9'), 7)
        self.assertEqual(parse_expression('3-5+9 -8+5+3-1-2+4'), 8)

    def test_simple_expression_with_multiplication(self):
        self.assertEqual(parse_expression('3*9'), 27)
        self.assertEqual(parse_expression('3*9*7'), 189)

    def test_expression_with_multiplication_divison_and_plus(self):
        self.assertEqual(parse_expression('3*9-3'), 24)
        self.assertEqual(parse_expression('3+3*9'), 30)
        self.assertEqual(parse_expression('-3+3*9'), 24)
        self.assertEqual(parse_expression('-4+9/3+2'), 1)
        self.assertEqual(parse_expression('-4+9/3*4+2'), 10)

    def test_expression_with_round_brackets(self):
        self.assertEqual(parse_expression('(-3)'), -3)
        self.assertEqual(parse_expression('(3 + 5)/2'), 4)
        self.assertEqual(parse_expression('(-3) + (-3)'), -6)
        self.assertEqual(parse_expression('(-3 + 4*9) + (-3)'), 30)
        self.assertEqual(parse_expression('(-3/(2-1) + 4*9) + (-3)'), 30)
        self.assertEqual(parse_expression('(1 + 2) * 4 / 2 + 1'), 7)
        self.assertEqual(parse_expression('(1 + 2) * -4 / 2 + 1'), -5)
        self.assertEqual(parse_expression('(-1 + 2) * -4 / 2 + 1'), -1)
